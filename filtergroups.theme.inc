<?php

/**
 * Render the exposed filters form with just plain drupal_render().
 */
function theme_filtergroups_exposed_form($element) {
  $form = $element['form'];
  unset($form['#theme']);
  return drupal_render($form);
}

/**
 * Tack the 'sort order' select element on to the 'sort by' form element.
 */
function theme_filtergroups_sort_widget($element) {
  unset($element['sort']['sort_order']['#theme_wrappers']);
  $element['sort']['sort_by']['#field_suffix'] = drupal_render($element['sort']['sort_order']);

  unset($element['sort']['#theme']);
  return drupal_render($element);
}

/**
 * Transform the filters and groups settings into a draggable table.
 */
function theme_filtergroups_options_form($element) {
  $element['form'] = _filtergroups_options_form_sort($element['form']);

  // Move the "new group" form to the bottom of the table, and force it to
  // render empty. It renders empty even when the form is rebuilt because of a
  // validation error.
  // @TODO How can this element be emptied in the normal form processing? It's
  //       already being emptied in the submit function...
  $new_group = $element['form']['new_group'];
  unset($element['form']['new_group']);
  foreach ($new_group['item']['#value'] as $key => $value) {
    if (isset($new_group[$key])) {
      $new_group[$key]['#value'] = $value;
    }
  }
  $element['form']['new_group'] = $new_group;

  $table_id = $element['form']['#id'];
  $weight_class = 'filtergroups-control-weight';
  $group_class = 'filtergroups-control-group';
  $parent_group_class = 'filtergroups-control-parent-group';

  drupal_add_tabledrag($table_id, 'match', 'parent', $parent_group_class, $parent_group_class, $group_class);
  drupal_add_tabledrag($table_id, 'order', 'sibling', $weight_class);

  foreach (element_children($element['form']) as $key) {
    $form_item = $element['form'][$key];
    $row = array(
      'data' => array(),
      'weight' => $form_item['#weight'],
      'class' => array(),
    );

    // If the item is a new group, it won't have a machine name. Don't make new
    // groups draggable.
    if (!empty($form_item['item']['#value']['machine_name'])) {
      $row['class'][] = 'draggable';
    }

    switch ($form_item['item']['#value']['type']) {
      case 'group':
        $indent = $type_label = '';
        $row['class'][] = 'tabledrag-root';
        break;
      default:
        $indent = !empty($form_item['item']['#value']['parent_group']) ? theme('indentation', array('size' =>  1)) : '';
        $type_label = '<strong>' . t(ucfirst($form_item['item']['#value']['type'])) . ':</strong> ';
        $row['class'][] = 'tabledrag-leaf';
    }

    // Minimize the machine name display.
    if ($form_item['machine_name']['#type'] == 'markup') {
      $form_item['machine_name']['#prefix'] = '<div class="description" style="display: inline; padding-left: .5em;">(';
      $form_item['machine_name']['#suffix'] = ')</div>';

      if ($form_item['item']['#value']['type'] == 'group') {
        $form_item['label']['#field_suffix'] = drupal_render($form_item['machine_name']);
      }
    }

    $form_item['weight']['#attributes']['class'][] = $weight_class;
    $form_item['group']['#attributes']['class'][] = $group_class;
    $form_item['parent_group']['#attributes']['class'][] = $parent_group_class;

    $row['data'] = array(
      'label' => $indent . $type_label . drupal_render($form_item['label']) . drupal_render($form_item['machine_name']),
      'collapse_condition' => drupal_render($form_item['collapse_condition']),
      'weight' => drupal_render($form_item['weight']),
      'parent_group' => drupal_render($form_item['group']) . drupal_render($form_item['parent_group']),
      'button' => drupal_render($form_item['button']),
    );

    $rows[] = $row;
  }

  return theme('table', array(
    'header' => array(t('Label'), t('Collapse condition'), t('Weight'), t('Group'), t('Action')),
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));
}

/**
 * $form is already sorted by #weight.
 */
function _filtergroups_options_form_sort($form) {
  $grouped_form = array();
  $children = array();

  foreach (element_children($form) as $key) {
    $form_item = $form[$key];
    $item = $form[$key]['item']['#value'];

    // Move groups to the grouped form.
    if ($item['type'] == 'group') {
      $grouped_form[$item['machine_name']] = array($key => $form_item);
    }
    // Move ungrouped items to the grouped form in their own groups.
    elseif (empty($item['parent_group'])) {
      $grouped_form[] = array($key => $form_item);
    }
    // Move everything that has a parent group set into the 'children' array.
    // This assumes that all children have valid groups set.
    else {
      $children[$key] = $form_item;
    }

    // Remove all element children from the form.
    unset($form[$key]);
  }

  // Move group children into the appropriate parent groups.
  foreach ($children as $key => $form_item) {
    $item = $form_item['item']['#value'];
    $grouped_form[$item['parent_group']][$key] = $form_item;
  }

  // Cobble the grouped form back onto the original form array with all of the
  // original non- element children properties.
  foreach ($grouped_form as $items) {
    $form += $items;
  }

  return $form;
}
