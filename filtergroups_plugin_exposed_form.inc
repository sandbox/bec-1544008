<?php

/**
 * @file
 */

/**
 * Exposed form plugin that provides a grouping for exposed form elements.
 *
 * @ingroup views_exposed_form_plugins
 */
class filtergroups_plugin_exposed_form extends views_plugin_exposed_form {

  function init(&$view, &$display, $options = array()) {
    parent::init($view, $display, $options);

    // For consistency with other types of Views plugins.
    $this->plugin_name = $this->definition['name'];

    // Break options out into useful configurations.
    $this->filtergroups = $this->filtergroups_options = $this->filtergroups_elements = array();
    foreach ($this->options['filtergroup_items'] as $item) {
      if ($item['type'] == 'group') {
        $this->filtergroups[$item['machine_name']] = $item;
        $this->filtergroups_options[$item['machine_name']] = $item['label'];
      }
      else {
        $this->filtergroups_elements[] = $item;
      }
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['filtergroup_items'] = array('default' => array());
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $this->form_path = views_ui_build_form_url($form_state);
    $form_state['plugin'] = $this;

    $form['filtergroup_items'] = array(
      '#tree' => TRUE,
      '#theme' => 'filtergroups_options_form',
      '#id' => drupal_html_id('filtergroups_options_form'),
    );

    $tracker = array(
      'group' => array(),
      'filter' => array(),
      'sort' => array(),
      'pager' => array(),
    );
    foreach ($this->options['filtergroup_items'] as $item) {
      $item = $this->clean_config($item);
      $form_key = $item['type'] . '__' . $item['machine_name'];
      $tracker[$item['type']][$item['machine_name']] = $form_key;
      $form['filtergroup_items'][$form_key] = $this->build_item_elements($item);
    }

    // Exposed filters.
    $display = $this->display->handler;
    foreach ($display->get_handlers('filter') as $machine_name => $filter) {
      // If this filter is exposed an not in the form yet, add it.
      if ($filter->options['exposed'] && empty($tracker['filter'][$machine_name])) {
        $item = $this->clean_config(array('type' => 'filter'), $filter->options);
        $form_key = $item['type'] . '__' . $item['machine_name'];
        $tracker['filter'][$machine_name] = $form_key;
        $form['filtergroup_items'][$form_key] = $this->build_item_elements($item);
      }
      // Otherwise if this filter is in the form but not exposed, remove it.
      elseif (!$filter->options['exposed'] && !empty($tracker['filter'][$machine_name])) {
        $form_key = $tracker['filter'][$machine_name];
        unset($form['filtergroup_items'][$form_key]);
        unset($tracker['filter'][$machine_name]);
      }
    }

    // Exposed sorts.
    $sort_exposed = FALSE;
    $machine_name = 'sort';
    foreach ($display->get_handlers('sort') as $sort) {
      if ($sort->options['exposed']) {
        $sort_exposed = TRUE;
        if (empty($tracker['sort'][$machine_name])) {
          $item = $this->clean_config(array('type' => 'sort'), $sort->options);
          $form_key = $item['type'] . '__' . $item['machine_name'];
          $tracker['sort'][$machine_name] = $form_key;
          $form['filtergroup_items'][$form_key] = $this->build_item_elements($item);
          break;
        }
      }
    }
    // If there are no exposed sorts but there is a sort in the tracker, remove
    // the sort from the form.
    if (!$sort_exposed && !empty($tracker['sort'][$machine_name])) {
      $form_key = $tracker['sort'][$machine_name];
      unset($form['filtergroup_items'][$form_key]);
      unset($tracker['sort'][$machine_name]);
    }

    // Exposed pager.
    $pager_exposed = FALSE;
    $machine_name = 'items_per_page';
    $pager_options = $display->get_option('pager');
    if (!empty($pager_options['options']['expose']['items_per_page'])) {
      $pager_exposed = TRUE;
      if (empty($tracker['pager'][$machine_name])) {
        $item = $this->clean_config(array('type' => 'pager'));
        $form_key = $item['type'] . '__' . $item['machine_name'];
        $tracker['pager'][$machine_name] = $form_key;
        $form['filtergroup_items'][$form_key] = $this->build_item_elements($item);
      }
    }

    if (!$pager_exposed && !empty($tracker['pager'][$machine_name])) {
      $form_key = $tracker['pager'][$machine_name];
      unset($form['filtergroup_items'][$form_key]);
      unset($tracker['pager'][$machine_name]);
    }

    // Add new group form.
    $item = $this->clean_config(array('type' => 'group', 'weight' => 50));
    $elements = $this->build_item_elements($item);
    $elements['#element_validate'] = array('_filtergroups_new_group_validate');
    $elements['#title'] = t('New group');
    $elements['label']['#title'] = t('New group');
    $elements['machine_name'] = array(
      '#type' => 'machine_name',
      '#title' => t('Machine name'),
      '#required' => FALSE,
      '#machine_name' => array(
        'exists' => '_filtergroups_machine_name_exists',
        'source' => array('options', 'exposed_form_options', 'filtergroup_items', 'new_group', 'label'),
      ),
      '#filtergroups_groups' => $this->filtergroups_options,
    );
    // @see views_handler_field_field::options_form()
    // @see views_ui_config_item_form_submit_temporary()
    $elements['button'] = array(
      '#type' => 'button',
      '#value' => t('Add'),
      '#name' => 'add_group',
      '#ajax' => array(
        'path' => $this->form_path,
      ),
      '#submit' => array('filtergroups_options_form_ajax'),
      '#executes_submit_callback' => TRUE,
    );
    $form['filtergroup_items']['new_group'] = $elements;
  }

  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);

    $filtergroup_items = array();
    foreach ($form_state['values']['exposed_form_options']['filtergroup_items'] as $form_key => $value) {
      $value += $value['item'];
      if (!empty($value['machine_name'])) {
        $filtergroup_items[$form_key] = $this->clean_config($value);
      }
    }

    if (!empty($form_state['triggering_element']['#name']) && ($form_state['triggering_element']['#name'] == 'new_group' || $form_state['triggering_element']['#name'] == 'remove_group')) {
      $parents = $form_state['triggering_element']['#parents'];
      array_pop($parents);
      $item = drupal_array_get_nested_value($form_state['values'], $parents);
      $item += $item['item'];

      switch ($form_state['triggering_element']['#name']) {
        case 'new_group':
          // If the "Add group" button was clicked, add the group and empty the
          // 'new group'.
          $filtergroup_items['group__' . $item['machine_name']] = $this->clean_config($item);
          $filtergroup_items['new_group'] = $item['item'];
          break;
        case 'remove_group':
          // If the "Remove group" button was clicked, remove the group and
          // orphan its children.
          foreach ($filtergroup_items as $key => $value) {
            if ($value['type'] == $item['type'] && $value['machine_name'] == $item['machine_name']) {
              unset($filtergroup_items[$key]);
            }
            elseif ($value['parent_group'] == $item['machine_name']) {
              $filtergroup_items[$key]['parent_group'] = NULL;
            }
          }
          break;
      }
    }

    uasort($filtergroup_items, 'drupal_sort_weight');
    $form_state['values']['exposed_form_options']['filtergroup_items'] = $filtergroup_items;
  }

  function clean_config($item, $data = NULL) {
    static $weight;
    $weight++;
    switch ($item['type']) {
      case 'group':
        $defaults = array(
          'type' => 'group',
          'label' => '',
          'machine_name' => '',
          'collapse_condition' => 'input',
          'weight' => $weight,
        );
        break;
      case 'filter':
        $defaults = array(
          'type' => 'filter',
          'label' => '',
          'machine_name' => 'oops',
          'weight' => $weight,
          'parent_group' => NULL,
        );
        if (!empty($data) && $handler = views_get_handler($data['table'], $data['field'], 'filter')) {
          $handler->init($this->view, $data);
          $defaults['label'] = $handler->ui_name(TRUE);
          $defaults['machine_name'] = $data['id'];
        }
        break;
      case 'sort':
        $defaults = array(
          'type' => 'sort',
          'label' => t('Exposed sorts widget'),
          'machine_name' => 'sort',
          'weight' => $weight,
          'parent_group' => NULL,
        );
        break;
      case 'pager':
        $defaults = array(
          'type' => 'pager',
          'label' => t('Items per page'),
          'machine_name' => 'items_per_page',
          'weight' => $weight,
          'parent_group' => NULL,
        );
        break;
      default:
        $defaults = array();
    }

    $item += $defaults;
    return array_intersect_key($item, $defaults);
  }

  function build_item_elements($item) {
    $elements = array(
      '#type' => 'fieldset',
      '#title' => $item['label'],
      '#weight' => $item['weight'],
      '#tree' => TRUE,
    );
    $elements['item'] = array(
      '#type' => 'value',
      '#value' => $item,
    );
    $elements['label'] = array(
      '#type' => 'markup',
      '#markup' => check_plain($item['label']),
    );
    $elements['machine_name'] = array(
      '#type' => 'markup',
      '#markup' => check_plain($item['machine_name']),
    );
    $elements['collapse_condition'] = array(
      '#type' => 'select',
      '#options' => array(
        'input' => t('Collapse when there is no user input'),
        'always' => t('Initially collapsed'),
        'never' => t('Initially expanded'),
        'no_collapse' => t('Not collapsible'),
      ),
      '#default_value' => !empty($item['collapse_condition']) ? $item['collapse_condition'] : 'input',
    );
    $elements['weight'] = array(
      '#type' => 'select',
      '#options' => range(-50, 50),
      '#default_value' => $item['weight'],
    );
    $elements['group'] = array(
      '#type' => 'hidden',
      '#value' => '',
    );
    $elements['parent_group'] = array(
      '#type' => 'hidden',
      '#default_value' => !empty($item['parent_group']) ? $item['parent_group'] : '',
    );
    $elements['button'] = array(
      '#type' => 'button',
      '#access' => FALSE,
    );

    // Adjust to item type.
    switch ($item['type']) {
      case 'group':
        $elements['label'] = array(
          '#type' => 'textfield',
          '#title' => t('Group label'),
          '#default_value' => $item['label'],
        );
        $elements['group']['#value'] = $item['machine_name'];
        $elements['button'] = array(
          '#type' => 'button',
          '#value' => t('Remove'),
          '#name' => 'remove_group',
          '#ajax' => array(
            'path' => $this->form_path,
          ),
          '#submit' => array('filtergroups_options_form_ajax'),
          '#executes_submit_callback' => TRUE,
          '#filtergroups_item' => $item,
        );
        break;
      case 'filter':
      case 'sort':
      case 'pager':
        $elements['collapse_condition']['#access'] = FALSE;
        $elements['remove']['#access'] = FALSE;
        break;
      default:
        $elements['#access'] = FALSE;
    }

    return $elements;
  }

  function exposed_form_alter(&$form, &$form_state) {
    parent::exposed_form_alter($form, $form_state);

    // Zap Views form theme functions that prevent normal form rearranging.
    $form['#theme'] = views_theme_functions('filtergroups_exposed_form', $this->view, $this->display);

    // Put form item titles back in their proper places.
    foreach ($form['#info'] as $item) {
      $form[$item['value']]['#title'] = $item['label'];

      if (isset($form[$item['operator']])) {
        // Add the filter label to the operator, and hide it on the value field.
        $form[$item['operator']]['#title'] = $item['label'];
        $form[$item['value']]['#title_display'] = 'invisible';

        // Group the operator and value fields in a container.
        $element = array(
          '#type' => 'container',
          '#attributes' => array('class' => array('filtergroups-exposed-operator-group')),
        );
        $element[$item['operator']] = $form[$item['operator']];
        $element[$item['value']] = $form[$item['value']];

        // Replace the elements with the container.
        unset($form[$item['operator']]);
        $form[$item['value']] = $element;
      }
    }

    // Rearrange the sort elements.
    if (isset($form['sort_by'])) {
      // Flag the sort in the exposed input array if the values are not defaults.
      $form['sort_by']['#default_value'] = !empty($form['sort_by']['#default_value']) ? $form['sort_by']['#default_value'] : key($form['sort_by']['#options']);
      if ((!empty($this->view->exposed_input['sort_by']) && $this->view->exposed_input['sort_by'] != $form['sort_by']['#default_value'])
          || !empty($this->view->exposed_input['sort_order']) && $this->view->exposed_input['sort_order'] != $form['sort_order']['#default_value']) {
        $this->view->exposed_input['sort'] = TRUE;
      }

      // Group the 'sort_by' and 'sort_order' elements together.
      $form['sort'] = array(
        '#type' => 'container',
        '#theme' => 'filtergroups_sort_widget',
        'sort_by' => $form['sort_by'],
        'sort_order' => $form['sort_order'],
      );
      unset($form['sort_by']);
      unset($form['sort_order']);
    }

    // Unset the sort in the exposed input array if the value is the default.
    if (isset($form['items_per_page'])
        && !empty($this->view->exposed_input['items_per_page'])
        && $this->view->exposed_input['items_per_page'] == $form['items_per_page']['#default_value']) {
      unset($this->view->exposed_input['items_per_page']);
    }

    // Make sure the submit and reset buttons come last on the form.
    $form['submit']['#weight'] = 500;
    if (!empty($form['reset'])) {
      $form['reset']['#weight'] = 500.1;
    }

    // Weight and group elements according to the filtergroup configuration.
    foreach ($this->filtergroups_elements as $item) {
      $info_key = $item['type'] . '-' . $item['machine_name'];
      if (isset($form['#info'][$info_key])) {
        $key = $form['#info'][$info_key]['value'];
        $key_op = $form['#info'][$info_key]['operator'];
      }
      else {
        $key = $item['machine_name'];
        $key_op = FALSE;
      }

      if (isset($form[$key])) {
        $form[$key]['#weight'] = $item['weight'];

        if (!empty($item['parent_group'])) {
          // Create the parent group if it does not already exist.
          $parent_item = $this->filtergroups[$item['parent_group']];
          $parent_group = $parent_item['machine_name'];
          if (empty($form[$parent_group])) {
            $form[$parent_group] = array(
              '#type' => 'fieldset',
              '#title' => $parent_item['label'],
              '#weight' => $parent_item['weight'],
              '#tree' => FALSE,
              '#collapsible' => ($parent_item['collapse_condition'] != 'no_collapse' ? TRUE : FALSE),
              '#collapsed' => ($parent_item['collapse_condition'] != 'never' ? TRUE : FALSE),
            );
          }

          // Move the item from the form into the parent group.
          $form[$parent_group][$key] = $form[$key];
          unset($form[$key]);

          // If this is a filter with an operator, move the operator into the
          // parent group.
          if ($key_op && isset($form[$key_op])) {
            $form[$key_op]['#weight'] = $item['weight'] - 0.1;
            $form[$parent_group][$key_op] = $form[$key_op];
            unset($form[$key_op]);
          }

          // Check for user input if the filter group is set to "Collapse when
          // there is no user input".
          if ($parent_item['collapse_condition'] == 'input') {
            $form[$parent_group]['#collapsed'] &= empty($this->view->exposed_input[$key]);
          }
        }
      }
    }
  }

}

function _filtergroups_machine_name_exists($machine_name, $element, $form) {
  return $machine_name && isset($element['#filtergroups_groups'][$machine_name]);
}

function _filtergroups_new_group_validate($element, &$form_state, $form) {
  // Clicked "Add group" but left the machine name blank.
  if (!empty($form_state['triggering_element']['#name']) && $form_state['triggering_element']['#name'] == 'add_group' && empty($element['machine_name']['#value'])) {
    form_error($element, t("If you're adding a new group, you must enter a group label and machine name."));
  }
  // Entered a label but no machine name and clicked either "Apply" or "Add group".
  elseif ((empty($form_state['triggering_element']['#name']) || $form_state['triggering_element']['#name'] != 'remove_group')
          && !empty($form_state['values']['exposed_form_options']['filtergroup_items']['new_group']['label'])
          && empty($form_state['values']['exposed_form_options']['filtergroup_items']['new_group']['machine_name'])) {
    form_error($element, t("If you're adding a new group, you must enter a group label and machine name."));
  }
}

/**
 * Views provides some functions for using ajax in handler configuration forms;
 * this uses a similar method to do ajax in the plugin configuration form.
 *
 * @see views_ui_config_item_form_submit_temporary().
 */
function filtergroups_options_form_ajax($form, &$form_state) {
  // Run the plugin's submit function.
  $display = &$form_state['view']->display[$form_state['display_id']];
  $display->handler->options_submit($form, $form_state);

  // Create a new plugin to use for storage, and unpack the options from the
  // form onto it.
  $plugin = views_get_plugin($form_state['plugin']->plugin_type, $form_state['plugin']->plugin_name);
  $plugin->init($form_state['view'], $form_state['plugin']->display, $form_state['plugin']->options);

  // Add the incoming options to existing options.
  $options = $form_state['values']['exposed_form_options'] + $form_state['plugin']->options;

  // This only unpacks options that are in the definition.
  $plugin->unpack_options($plugin->options, $options, NULL, FALSE);

  // Store the item back on the view.
  $form_state['view']->temporary_options[$form_state['plugin']->plugin_type][$form_state['id']] = $plugin->options;

  // Add this form back to the top of the stack.
  views_ui_add_form_to_stack($form_state['form_key'], $form_state['view'], $form_state['display_id'], $form_state['build_info']['args'], TRUE);

  $form_state['rerender'] = TRUE;
  $form_state['rebuild'] = TRUE;

  // Write the temporary view to the cache.
  views_ui_cache_set($form_state['view']);
}
